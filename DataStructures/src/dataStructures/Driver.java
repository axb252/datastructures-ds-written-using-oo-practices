package dataStructures;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.TreeMap;

public class Driver {

	//class variable that holds two integers
	//the first represents the global ID of the card, and the second represents the index in the array
	
	//NOW I FULLY UNDERSTAND WHAT YOU MEANT BY YOUR HASHING QUESTION...
	//EACH NEWLY CREATED CARD WOULD POINT TO A DIFFERENT OBJECT EVEN THOUGH THE RANK AND COLOR WAS THE SAME
	//HENCE WE HAVE TO USE THE GLOBAL ID
	static TreeMap<Integer, Integer> mapOfCards = new TreeMap<Integer, Integer>();
	
	public static void main(String[] args) {
		
		//create card objects
		Card c1 = new Card("red", 4, "c1");
		Card c2 = new Card("blue", 4, "c2");
		Card c3 = new Card("red", 4, "c3");
		Card c4 = new Card("blue", 5, "c4");
		Card c5 = new Card("red", 6, "c5");
	
		Card[] arrayOfCards = {c1,c2,c3,c4,c5};
		
		//iterate through array to make sure the duplicate cards have the same global ids
		/*for(Card c: arrayOfCards){
			System.out.println(c.getGLOBALID());
		}
		
		//perform the duplication check and print each card in the array
		for(Card c : checkDups(arrayOfCards)){
			System.out.println(c.toString());
		}*/
		
	/*List list = new List();
	Object[] a = {5, "hello", c5, c3}; 
	List list2 = new List(a);

	list.add(5);
	list.add("hello");
	list.add(c5);
	
	for(Object o : list) {
		System.out.println(o + " and size is: "  + list.size());
	}
	for(Object o : list2) {
		System.out.println(o + " and size is: "  + list2.size());
	}
	*/
	Stack stack = new Stack();
	stack.push(5);
	System.out.println("Stack with one element- \n" + stack.toString());
	stack.push(6);
	System.out.println(stack.toString());
	stack.push(2);
	System.out.println(stack.toString());
	stack.push(4);
	System.out.println(stack.toString());
	stack.push(8);
	System.out.println(stack.toString());
	
		
	Queue queue = new Queue();
	queue.enqueue(c1);
	queue.enqueue("hi");
	queue.enqueue(5);
	System.out.println("Queue before dequeueing- \n" + queue.toString());
	queue.dequeue();
	System.out.println("Queue after 1 dequeue- \n" + queue.toString());
	
	}

	private static ArrayList<Card> checkDups(Card[] c) {
		
		//list to hold duplicates
		ArrayList<Card> list = new ArrayList<Card>();
		
		//this loop iterates through the card array, and if the map doesn't have the card global id
		//in it yet, it adds the index; else it adds the card to the list because we know it is a duplicate
		//there is also a boolean check to see if the original index card was added
		for(int i = 0; i < c.length;i++){
			if(mapOfCards.get(c[i].getGLOBALID()) == null){
				mapOfCards.put(c[i].getGLOBALID(), i);
			}
			else if(mapOfCards.get(c[i].getGLOBALID()) != null){
				if(!list.contains(c[mapOfCards.get(c[i].getGLOBALID())])) {
					list.add(c[mapOfCards.get(c[i].getGLOBALID())]);
				}
			}
		}
		return list;
	}



}