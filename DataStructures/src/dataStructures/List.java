package dataStructures;

import java.util.Iterator;
import java.util.NoSuchElementException;

/*
 * This List data structure is designed to be an introduction into node-based structures.
 * 
 * It will have extremely simple functionality: add objects to the front of the list
 * and be able to iterate through the list (no removing functionality).
 * 
 * This structure will be used later in our Graph structures as adjacency lists, but that is
 * later so don't worry about it for now. 
 */
public class List<AnyType> implements Iterable<AnyType> {
	
	/*****************************************************
	 * class variables and inner classes
	 ****************************************************/
	
	private int size = 0; //size of the list
	private Node<AnyType> first; //first object in the list
	
	//Node class that holds an AnyType object and points to another Node
	private static class Node<AnyType> {
		private AnyType element;
		private Node<AnyType> next;
	}
	
	//iterator functionality
	public Iterator<AnyType> iterator() {
		return new CustomIterator<AnyType>(this.getFirst());
	}
	
	//separating iterator functionality from class for organization
	private class CustomIterator<AnyType> implements Iterator<AnyType> {
		private Node<AnyType> pointer;
		public CustomIterator(Node<AnyType> first) {
			pointer = first;
		}
		
		public boolean hasNext() {
			return pointer != null;
		}
		
		//this basic List data structure doesn't support removing functionality
		public void remove() {
			throw new UnsupportedOperationException();
		}
		
		public AnyType next() {
			//first check to make there is a next element
			if(!hasNext()) throw new NoSuchElementException();
			AnyType element = pointer.element;
			
			//move the pointer to the next element and return the current element
			pointer = pointer.next;
			return element;
		}
	}
	
	/*****************************************************
	 * constructors
	 ****************************************************/
	
	//initialize empty List
	public List() {
		this.first = null;
	}
	
	//initialize List with given array of AnyType objects
	public List(AnyType[] a) {
		for(int i = 0;i<a.length;i++){
			this.add(a[i]);
		}
	}
	
	/*****************************************************
	 * mutators (object oriented getters / setters)
	 ****************************************************/
	
	public int getSize() {
		return this.size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public Node<AnyType> getFirst() {
		return this.first;
	}

	public void setFirst(Node<AnyType> first) {
		this.first = first;
	}
	
	/*****************************************************
	 * User-defined methods
	 ****************************************************/
	
	//is List empty?
	public boolean isEmpty() {
		return this.getFirst() == null;
	}
	
	//more client friendly get size method
	public int size() {
		return this.getSize();
	}
	
	//add an AnyType object to the List
	public void add(AnyType element) {
		
		//set the old first to another Node
		Node<AnyType> former = this.getFirst(); 
		
		//set the first Node to the passed in element and the pointer to the old first
		this.setFirst(new Node<AnyType>());
		this.getFirst().element = element;
		this.getFirst().next = former;
		
		//increment size accordingly
		this.setSize(this.getSize()+1);
	}
	
}
