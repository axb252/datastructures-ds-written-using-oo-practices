package dataStructures;

public class Card {

	/*
	 * class variables
	 */
	
	private int rank;
	private String color;
	private String name;
	private int GLOBALID; //a unique identifier for all cards with same color and rank
	
	/*
	 * constructors
	 */

	public Card(String color, int rank, String name) {
		this.color = color;
		this.rank = rank;
		this.name = name;
		this.setGLOBALID(this.getGLOBALID());
	}
	
	public Card(String color, int rank) {
		this.color = color;
		this.rank = rank;
		this.setGLOBALID(this.getGLOBALID());
	}

	
	/*
	 * mutators
	 */

	//for simplicity assuming maximum of 13 cards
	//can alter this as necessary (hash if need to)
	public int getGLOBALID() {
		int id = 0;
		if(this.getColor() == "red"){
			id = rank;
		}
		else if(this.getColor()=="blue"){
			id = rank + 13;
		}
		else if(this.getColor()=="green"){
			id = rank + 26;
		}
		else if(this.getColor() == "yellow"){
			id = rank + 39;
		}
		return id;
	}

	public void setGLOBALID(int gLOBALID) {
		this.GLOBALID = gLOBALID;
	}
	
	public int getRank() {
		return this.rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public String getColor() {
		return this.color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	

	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}
	
	
	/*
	 * user defined methods
	 */

	@Override
	public String toString() {
		String s = "";
		s = this.getName() + " Color: " + this.getColor() + " Rank: " + this.getRank();
		return s;
	}
	
	
}
