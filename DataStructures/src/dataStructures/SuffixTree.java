package dataStructures;

import java.util.ArrayList;

import chapter20Hard.SuffixTreeNode;

public class SuffixTree {
	
	/*
	 * class variables
	 */
	
	SuffixTreeNode root = new SuffixTreeNode();
	
	/*
	 * constructors
	 */
	
	public SuffixTree(String s) {
		for(int i = 0; i < s.length();i++){
			String suffix = s.substring(i);
			root.insertString(suffix, i);
		}
	}
	
	/*
	 * mutators
	 */
	
	public SuffixTreeNode getRoot() {
		return this.root;
	}

	public void setRoot(SuffixTreeNode root) {
		this.root = root;
	}
	
	/*
	 * user defined methods
	 */
	
	public ArrayList<Integer> getIndexes(String s) {
		return this.getRoot().getIndexes(s);
	}
	
	
}

