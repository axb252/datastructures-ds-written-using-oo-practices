package dataStructures;

public class Graph {
	//class variables
	private final int V;
	private int E;
	private List<Integer>[] adj;
	
	//constructors
	public Graph(int V){
		if(V < 0) throw new IllegalArgumentException("Number of vertices must be 0");
		this.V = V;
		this.E = 0;
		adj = (List<Integer>[]) new List[V];
		for(int v = 0; v < V;v++){
			adj[v] = new List<Integer>();
		}
	}
	
	//make a deep copy of a graph
	public Graph(Graph G){
		this(G.getV());
		this.E = G.getE();
		for(int v = 0;v < G.V;v++){
			//reverse so that adjacency list is in same order as original
			Stack<Integer> reverse = new Stack<Integer>();
			for(int w : G.adj[v]){
				reverse.push(w);
			}
			for(int w : reverse){
				adj[v].add(w);
			}
		}
	}
	
	//mutators
	public int getE() {
		return this.E;
	}

	public void setE(int e) {
		this.E = e;
	}

	public int getV() {
		return this.V;
	}
	
	//user defined methods
	
	//add an undirected edge v-w to graph
	public void addEdge (int v, int w){
		if(v<0 || v >= this.getV()) throw new IndexOutOfBoundsException();
		if(w<0 || w >= this.getV()) throw new IndexOutOfBoundsException();
		
		this.setE(this.getE()+1);
		adj[v].add(w);
		adj[w].add(v);
	}
	
	//return adjacent vertices

	public Iterable<Integer> adj (int v) {
		if(v < 0 || v >= this.getV()) throw new IndexOutOfBoundsException();
		return adj[v];
	}
	
	//return a string representation of the graph
	public String toString() {
		StringBuilder s = new StringBuilder();
		String NEWLINE = System.getProperty("line.separator");
		s.append(this.getV() + " vertices, " + this.getE() + " edges " + NEWLINE);
		for(int v = 0; v < this.getV();v++){
			s.append(v + ": ");
			for(int w : adj[v]) {
				s.append(w + " ");
			}
			s.append(NEWLINE);
		}
		return s.toString();
	}
	
}










