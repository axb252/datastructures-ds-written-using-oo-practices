package dataStructures;

import java.util.ArrayList;
import java.util.HashMap;

import javax.print.attribute.HashAttributeSet;

public class SuffixTreeNode {
	
	/*
	 * class variables
	 */
	
	HashMap<Character, SuffixTreeNode> children = new HashMap<Character, SuffixTreeNode>();
	char value;
	ArrayList<Integer> indices = new ArrayList<Integer>();
	
	/*
	 * constructors
	 */
	
	public SuffixTreeNode(){}
	
	/*
	 * mutators
	 */
	
	public HashMap<Character, SuffixTreeNode> getChildren() {
		return children;
	}
	
	public void setChildren(HashMap<Character, SuffixTreeNode> children) {
		this.children = children;
	}

	public char getValue() {
		return this.value;
	}

	public void setValue(char value) {
		this.value = value;
	}

	public ArrayList<Integer> getIndices() {
		return this.indices;
	}

	public void setIndices(ArrayList<Integer> indices) {
		this.indices = indices;
	}
	
	/*
	 * user defined methods
	 */
	
	public void insertString(String s, int index) {
		this.getIndices().add(index);
		if(s != null && s.length() > 0){
			this.setValue(s.charAt(0));
			SuffixTreeNode child = null;
			if(this.getChildren().containsKey(this.getValue())) {
				child = this.getChildren().get(this.getValue());
			}
			else {
				child = new SuffixTreeNode();
				this.getChildren().put(this.getValue(), child);
			}
			String remainder = s.substring(1);
			child.insertString(remainder, index);
		}
	}

	public ArrayList<Integer> getIndexes(String s) {
		if(s==null || s.length()==0) {
			return this.getIndices();
		}
		else {
			char first = s.charAt(0);
			if(this.getChildren().containsKey(first)) {
				String remainder = s.substring(1);
				return this.getChildren().get(first).getIndexes(remainder);
			}
		}
		return null;
	}
	
	
	
	
	
	
	
	
}
