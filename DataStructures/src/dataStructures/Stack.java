package dataStructures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Stack<AnyType extends Comparable<AnyType>> implements
		Iterable<AnyType> {

	/*****************************************************
	 * class variables and inner classes
	 ****************************************************/

	private int size = 0; // size of stack
	private Node first; // top of the stack
	AnyType currentMin; // current Node in stack that holds the minimum value
	int capacity; // capacity of stack

	// inner class that defines Node concept, holding a value and pointing to
	// next node, as well as keeping track of the current minimum Node
	public class Node<AnyType extends Comparable<AnyType>> {
		AnyType element;
		Node next;
		AnyType currentMinBelowNode;

		public AnyType getCurrentMinBelowNode() {
			return this.currentMinBelowNode;
		}

		public void setCurrentMinBelowNode(AnyType currentMinBelowNode) {
			this.currentMinBelowNode = currentMinBelowNode;
		}
	}

	// custom iterator functionality
	public Iterator<AnyType> iterator() {
		return new CustomIterator<AnyType>(this.getFirst());
	}

	private class CustomIterator<AnyType> implements Iterator<AnyType> {
		private Node pointer = first;

		public CustomIterator(Node first) {
			pointer = first;
		}

		public boolean hasNext() {
			return pointer != null;
		}

		public void remove() {
			throw new UnsupportedOperationException();
		}

		public AnyType next() {
			// first check to make there is a next element
			if (!hasNext())
				throw new NoSuchElementException();
			AnyType element = (AnyType) pointer.element;
			pointer = pointer.next;
			return element;
		}
	}

	/*****************************************************
	 * constructors
	 ****************************************************/

	// initialize stack with default capacity of 5
	public Stack() {
		super();
		this.first = null;
		this.size = 0;
		this.capacity = 5;
	}

	// initialize stack with given capacity
	public Stack(int capacity) {
		super();
		this.first = null;
		this.size = 0;
		this.capacity = capacity;
	}

	/*****************************************************
	 * mutators (object oriented getters / setters)
	 ****************************************************/

	public int getSize() {
		return this.size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public Node<AnyType> getFirst() {
		return this.first;
	}

	public void setFirst(Node first) {
		this.first = first;
	}

	public AnyType getCurrentMin() {
		return this.getFirst().currentMinBelowNode;
	}

	public void setCurrentMin(AnyType currentMin) {
		this.currentMin = (AnyType) this.first.currentMinBelowNode;
	}

	public int getCapacity() {
		return this.capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	/*****************************************************
	 * User-defined methods
	 ****************************************************/

	// add anyType to top of stack
	public void push(AnyType element) {
		Node<AnyType> former = this.getFirst();
		this.setFirst(new Node<AnyType>());
		this.getFirst().element = element;
		this.getFirst().next = former;

		this.setSize(this.getSize() + 1);

		// if first element, is min
		if (this.getSize() == 1) {
			this.getFirst().setCurrentMinBelowNode(element);
		} else {
			// if more than 1 object in stack and the current first is greater
			// than the object
			if (this.getFirst().next.getCurrentMinBelowNode()
					.compareTo(element) > 0) {
				this.getFirst().setCurrentMinBelowNode(element);
			} else {
				this.getFirst()
						.setCurrentMinBelowNode(
								(AnyType) this.getFirst().next
										.getCurrentMinBelowNode());
			}
		}
		// set current min to highest node's min
		this.setCurrentMin((AnyType) this.getFirst().getCurrentMinBelowNode());
	}

	// remove object from top of stack
	public AnyType pop() {
		if (this.isEmpty()) {
			throw new NoSuchElementException(
					"There isn't an item in the stack to pop!");
		}
		AnyType element = (AnyType) this.getFirst().element;
		this.setFirst(this.getFirst().next);
		this.setSize(this.getSize() - 1);

		// have to make sure the stack isn't empty after pop, otherwise can't
		// set to lower node's min
		if (this.getSize() > 0) {
			this.setCurrentMin((AnyType) first.getCurrentMinBelowNode());
		}

		return element;
	}

	public boolean isEmpty() {
		return this.getFirst() == null;
	}

	// look at first object in stack
	public AnyType peek() {
		if (this.isEmpty()) {
			throw new NoSuchElementException(
					"There isn't an item in the stack to pop!");
		}
		return (AnyType) this.getFirst().element;
	}

	// more friendly size method for client
	public int size() {
		return this.getSize();
	}

	// display stack contents in order on separate lines
	public String toString() {
		String s = "";
		int counter = 1;
		for (Object o : this) {
			s += counter + ": " + o + " the minimum value in the stack is: "
					+ this.getCurrentMin() + "\n";
			counter++;
		}
		return s;
	}

}
