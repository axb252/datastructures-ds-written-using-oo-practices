package dataStructures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Queue<AnyType> implements Iterable<AnyType> {

	/*****************************************************
	 * class variables and inner classes
	 ****************************************************/

	private Node<AnyType> first; // link to most recently added node
	private Node<AnyType> last; // link to last added node
	private int size = 0; // number of elements

	// nested class to define nodes, which hold an object and point to the next
	// node
	private class Node<AnyType> {
		AnyType element;
		Node<AnyType> next;
	}

	// iterator functionality to be able to iterate through structure using
	// standard foreach procedures
	public Iterator<AnyType> iterator() {
		return new CustomIterator();
	}

	private class CustomIterator implements Iterator<AnyType> {
		private Node pointer = first;

		public boolean hasNext() {
			return pointer != null;
		}

		public void remove() {
			throw new UnsupportedOperationException();
		}

		public AnyType next() {
			AnyType element = (AnyType) pointer.element;
			pointer = pointer.next;
			return element;
		}
	}

	/*****************************************************
	 * Constructors
	 ****************************************************/

	public Queue() {
		this.first = null;
		this.last = null;
		this.size = 0;
	}

	/*****************************************************
	 * Mutators (Object-Oriented getters and setters)
	 ****************************************************/

	public Node<AnyType> getFirst() {
		return this.first;
	}

	public void setFirst(Node<AnyType> first) {
		this.first = first;
	}

	public Node<AnyType> getLast() {
		return this.last;
	}

	public void setLast(Node<AnyType> last) {
		this.last = last;
	}

	public int getSize() {
		return this.size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	/*****************************************************
	 * User-defined methods
	 ****************************************************/

	// add element to end of list
	public void enqueue(AnyType element) {

		Node<AnyType> former = this.getLast();
		this.setLast(new Node<AnyType>());
		this.getLast().element = element;
		this.getLast().next = null;
		if (this.isEmpty()) {
			this.setFirst(this.getLast());
		} else {
			former.next = this.getLast();
		}
		this.setSize(this.getSize() + 1);
	}

	// remove element from beginning of list
	public AnyType dequeue() {

		AnyType element = (AnyType) this.getFirst().element;
		this.setFirst(this.getFirst().next);
		if (this.isEmpty()) {
			this.setLast(null);
		}
		this.setSize(this.getSize() - 1);
		return element;
	}

	public boolean isEmpty() {
		return this.getFirst() == null;
	}

	public int size() {
		return this.getSize();
	}

	// look at first object in queue
	public AnyType peek() {
		if (this.isEmpty()) {
			throw new NoSuchElementException(
					"There isn't an item in the queue to enqueue!");
		}
		return (AnyType) this.getFirst().element;
	}

	// display stack contents in order on separate lines
	public String toString() {
		String s = "";
		int counter = 1;
		for (Object o : this) {
			s += counter + ": " + o + "\n";
			counter++;
		}
		return s;
	}
}
