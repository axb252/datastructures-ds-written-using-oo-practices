package dataStructures;

import java.util.ArrayList;
import java.util.LinkedList;

public class BinaryTree<Item extends Comparable<Item>>{
	
	//class variables
	private Node root;
	private int N;
	
	//private Node class to represent dual linked nature
	public class Node<Item extends Comparable<Item>> {
		Node left;
		Node right;
		Item data;
		
		public Node(Item data){
			this.left = null;
			this.right = null;
			this.data = data;
		}
	}
	
	//creates a null tree
	public void BinaryTree() {
		root = null;
		N = 0;
	}
	
	/*user defined methods
	 * one with public client method, one with private recursive version 
	 */
	
	//lookup method client sees
	public boolean lookup(Item data){
		return (lookup(root, data));
	}

	//recursive lookup that finds node that holds item
	private boolean lookup(Node node, Item data) {
		if(node == null) {
			return(false);
		}
		if(data.equals(node.data)){
			return(true);
		}
		else if(data.compareTo((Item) node.data) == -1){
			return (lookup(node.left, data));
		}
		else {
			return(lookup(node.right, data));
		}
	}
	
	//insert method client uses with only data
	public void insert (Item data) {
		
		root = insert(root, data);
		N++;
	}

	//insert method that recursively inserts the inputted item with appropriate node
	private Node insert(Node node, Item data) {
		if(node==null){
			node = new Node(data);
		}
		else {
			if(data.compareTo((Item) node.data) <= 0){
				node.left = insert(node.left,data);
			}
			else {
				node.right = insert(node.right, data);
			}
		}
		return(node);
	}
	
	//client size method that calls recursive size method
	public int size() {
		return size(root);
	}
	
	//recursive size method that adds left, self, and right nodes until null reached for both sides
	private int size(Node node) {
		if(node==null) {
			return 0;
		}
		else {
			return (size(node.left) + 1 + size(node.right));
		}
	}
	
	//client maxDepth method that calls recursive maxDepth method
	public int maxDepth() {
		return maxDepth(root);
	}

	//recursive maxDepth method that searches for depth recursively down tree
	private int maxDepth(Node node) {
		if(node == null){
			return 0;
		}
		else {
			int lDepth = maxDepth(node.left);
			int rDepth = maxDepth(node.right);
			
			//use larger + 1
			
			return (Math.max(lDepth, rDepth) + 1);
		}
	}
	
	//client minDepth method that calls recursive mminDepth method
	public int minDepth() {
		return minDepth(root);
	}

	//recursive minDepth method that searches for depth recursively down tree
	private int minDepth(Node node) {
		if(node == null){
			return 0;
		}
		else {
			int lDepth = minDepth(node.left);
			int rDepth = minDepth(node.right);
			
			//use smaller + 1
			
			return (Math.min(lDepth, rDepth) + 1);
		}
	}
	
	//get min value in non empty binary tree
	public Item minValue() {
		return minValue(root);
	}

	private Item minValue(Node node) {
		Node current = node;
		while(current.left!=null){
			current = current.left;
		}
		return (Item) current.data;
	}
	
	//get max value in a non empty binary tree
	public Item maxValue() {
		return maxValue(root);
	}

	private Item maxValue(Node node) {
		Node current = node;
		while(current.right!=null){
			current = current.right;
		}
		return (Item) current.data;
	}
	
	//print tree in order
	public void printTree() {
		System.out.println("In order: ");
		printTree(root);
		System.out.println();
	}

	private void printTree(Node node) {
		if(node==null) return;
		//left, node itself, right
		printTree(node.left);
		System.out.print(node.data + " ");
		printTree(node.right);
	}
	
	//print tree in post order, left, right, current node
	public void printPostorder(){
		System.out.println("Post order: ");
		printPostorder(root);
		System.out.println();
	}

	private void printPostorder(Node node) {
		if(node == null) return;
		
		//first recur on both subtrees
		printPostorder(node.left);
		printPostorder(node.right);
		
		//then print current node
		System.out.print(node.data + " ");
	}
	
	//print tree in pre order, current node, left, right
	public void printPreorder(){
		System.out.println("Pre order: ");
		printPreorder(root);
		System.out.println();
	}

	private void printPreorder(Node node) {
		if(node == null) return;
		
		//first print current node
		System.out.print(node.data + " ");
		
		//then recur on both subtrees
		printPreorder(node.left);
		printPreorder(node.right);
		
	
	}
	
	//print tree in level order
	//this is breadth first traversal
	public void printLevelorder(){
		System.out.println("Level order: ");
		printLevelorder(root);
		System.out.println();
	}

	private void printLevelorder(Node root) {
		if(root == null) return;
		Queue<Node> nodeQueue = new Queue<Node>();
		Node current = root;
		nodeQueue.enqueue(current);
		
		while(!nodeQueue.isEmpty()){
			current = nodeQueue.dequeue();
			System.out.print(current.data + " ");
			
			if(current.left!=null){
				nodeQueue.enqueue(current.left);
			}
			if(current.right!=null){
				nodeQueue.enqueue(current.right);
			}
		}
		
	}
	
	public ArrayList<LinkedList<Node>> findLevelLinkList() {
		return findLevelLinkList(root);
	}
	
	private ArrayList<LinkedList<Node>> findLevelLinkList(Node root) {
		int level = 0;
		ArrayList<LinkedList<Node>> result = new ArrayList<LinkedList<Node>>();
		LinkedList<Node> list = new LinkedList<Node>();
		list.add(root);
		result.add(level, list);
		while(true) {
			list = new LinkedList<Node>();
			for(int i = 0; i < result.get(level).size(); i++){
				Node n = (Node) result.get(level).get(i);
				if(n!= null){
					if(n.left!=null) list.add(n.left);
					if(n.right!=null) list.add(n.right);
				}
			}
			if(list.size() > 0){
				result.add(level+1, list);
			}
			else {
				break;
			}
			level++;
		}
		return result;
	}

	//return whether or not tree has a path that equals a particular passed in sum
	public boolean hasPathSum (Integer sum){
		return hasPathSum(root, sum);
	}

	private boolean hasPathSum(Node node, Integer sum) {
		//return true if we run out of tree and sum == 0
		if(node == null){
			return sum==0;
		}
		else {
			//otherwise check both subtrees
			Integer subSum = sum - (Integer)node.data;
			return (hasPathSum(node.left, subSum) || hasPathSum(node.right, subSum));
		}
	}
	
	//print paths
	public void printPaths(){
		int [] path = new int[1000];
		printPaths(root, path, 0);
	}

	private void printPaths(Node node, int[] path, int pathLen) {
		if(node==null) return;
		
		//append node to path array 
		path[pathLen] = (Integer) node.data;
		pathLen++;
		
		//it's a leaf, so print path that led to there
		if(node.left == null && node.right == null){
			printArray(path,pathLen);
		}
		
		//else try both subtrees
		else {
			printPaths(node.left,path, pathLen);
			printPaths(node.right,path,pathLen);
		}
	}

	private void printArray(int[] ints, int len) {
		for(int i = 0;i < len;i++){
			System.out.print(ints[i] + " ");
		}
		System.out.println();
		
	}
	
	//print the mirror of a tree
	public void mirror() {
		mirror(root);
	}

	private void mirror(Node node) {
		if(node!=null){
			//do sub trees
			mirror(node.left);
			mirror(node.right);
			
			//swap left / right pointers
			Node temp = node.left;
			node.left = node.right;
			node.right = temp;
		}
	}
	
	//insert duplicate node into each node's .left
	public void doubleTree(){
		doubleTree(root);
	}
	
	private void doubleTree(Node node){
		Node oldLeft;
		
		if(node == null) return;
		
		//do the subtrees
		doubleTree(node.left);
		doubleTree(node.right);
		
		//duplicate this node to its left
		oldLeft = node.left;
		node.left = new Node (node.data);
		node.left.left = oldLeft;
	}
	
	//compares trees to see if same
	public boolean sameTree(BinaryTree other){
		return sameTree(root, other.root);
	}

	private boolean sameTree(Node a, Node b) {
		//if both are empty return true
		if(a == null && b == null) return true;
		
		//both are non empty so compare
		else if (a!= null && b!= null){
			return(a.data == b.data && sameTree(a.left, b.left) && sameTree(a.right, b.right));
		}
		//else one is empty one is not so false
		else {
			return false;
		}	
	}
	
	//find number of structurally unique trees possible given number of keys
	public static int countTrees(int numKeys){
		if (numKeys<= 1){
			return 1;
		}
		else {
			int sum = 0;
			int left, right, root;
			for(root =1;root <= numKeys;root++){
				left = countTrees(root-1);
				right = countTrees(numKeys - root);
				
				//number of possible trees is left times right
				 sum +=left*right;
			}
			return sum;
		}
	}
	
	//tests if a tree is a binary search tree
	public boolean isBST(){
		return (isBST(root));
	}

	private boolean isBST(Node node) {
		if(node == null) return true;
		
		//do subtrees contain values that agree with the node
		if(node.left!=null && maxValue(node.left).compareTo((Item) node.data) >= 1 ) return false;
		if(node.right != null && minValue(node.right).compareTo((Item) node.right) <= 0) return false;
		
		//check subtrees themselves
		return (isBST(node.left) && isBST(node.right));
	}
	
	//efficient order of N isBST method
	
	public boolean isBST2() {
		return (isBST2(root, Integer.MIN_VALUE, Integer.MAX_VALUE));
	}

	private boolean isBST2(Node node, int min, int max) {
		if(node == null) {
			return true;
		}
		else {
			//left should be in range min - node.data
			boolean leftOk = isBST2(node.left,min, (Integer)node.data);
			
			//if not ok, return false
			if(!leftOk) return false;
			
			//right should be in range node.data + 1 - max
			boolean rightOk = isBST2(node.right,(Integer)node.data +1, max);
			
			return rightOk;
		}
		
	}
	
}

