package dataStructures;

public class HashTable<Key, Value> {
	//class variables
	private static final int INIT_CAPACITY = 4;
	
	private int N;
	private int M;
	private Key[] keys;
	private Value[] vals;
	
	//constructors
	public HashTable() {
		this(INIT_CAPACITY);
	}
	
	public HashTable(int capacity) {
		this.M = capacity;
		this.keys = (Key[]) new Object[M];
		this.vals = (Value[]) new Object[M];
	}
	
	//mutators
	
	public int getN() {
		return N;
	}

	public void setN(int n) {
		N = n;
	}

	public int getM() {
		return M;
	}

	public void setM(int m) {
		M = m;
	}

	public Key[] getKeys() {
		return keys;
	}

	public void setKeys(Key[] keys) {
		this.keys = keys;
	}
	public void setKey(int i, Key key) {
		this.keys[i] = key;
	}

	public Value[] getVals() {
		return vals;
	}

	public void setVals(Value[] vals) {
		this.vals = vals;
	}
	
	public void setVal(int i, Value val) {
		this.vals[i] = val;
	}

	public static int getInitCapacity() {
		return INIT_CAPACITY;
	}
	
	//user defined methods
	
	//return number of key value pairs in symbol table
	public int size() {
		return this.getN();
	}
	
	//is symbol table empty?
	public boolean isEmpty() {
		return this.size() == 0;
	}
	
	//does it contain the key?
	public boolean contains(Key key){
		return this.get(key) != null;
	}
	
	//hash function for keys, reutrn value between 0 and M - 1
	private int hash(Key key) {
		return (key.hashCode() & 0x7fffffff) % this.getM();
	}
	
	//resize table to given capacity by rehashing all keys
	private void resize(int capacity){
		HashTable<Key, Value> temp = new HashTable<Key, Value>(capacity);
		for(int i = 0; i < this.getM(); i++){
			if(this.getKeys()[i] != null) {
				temp.put(this.getKeys()[i], this.getVals()[i]);
			}
		}
		this.setKeys(temp.keys);
		this.setVals(temp.vals);
		this.setM(temp.M);
	}
	
	//insert key value pair into table
	public void put(Key key, Value val) {
		if(val == null) delete(key);
		
		//double table size if 50% full
		if(this.getN() >= this.getM()/2) resize(2 * this.getM());
		
		int i;
		for(i = hash(key); keys[i] != null; i = (i + 1) % this.getM()) {
			if(this.getKeys()[i].equals(key)) {
				this.getVals()[i] = val;
				return;
			}
		}
		this.getKeys()[i] = key;
		this.getVals()[i] = val;
		this.setN(this.getN()+1);
	}
	
	//return value associated with key, else null
	public Value get(Key key) {
		for(int i = hash(key); this.getKeys()[i] != null; i = (i+1) % this.getM()){
			if(this.getKeys()[i].equals(key)) {
				return this.getVals()[i];
			}
		}
		return null;
	}
	
	//delete key and associated value
	public void delete(Key key) {
		if(!contains(key)) return;
		
		int i = hash(key);
		while(!key.equals(this.getKeys()[i])) {
			i = (i+1) % this.getM();
		}
		
		//delete key and associated value
		this.setKey(i, null);
		this.setVal(i, null);
		
		//rehash all keys in same cluster
		i = (i+1) % this.getM();
		while(this.getKeys()[i] != null) {
			//delete keys[i] and vals[i] and reinsert
			Key keyToRehash = this.getKeys()[i];
			Value valToRehash = this.getVals()[i];
			this.setKey(i, null);
			this.setVal(i, null);
			this.setN(this.getN()-1);
			put(keyToRehash, valToRehash);
			i = (i+1) % this.getM();
		}
		this.setN(this.getN()-1);
		
		//halves size of array if it's 12.5% or less full
		if(this.getN()>0 && this.getN() <= this.getM()/8) resize(this.getM()/2);
		
		assert check();
		
	}
	
	public Iterable<Key> keys() {
		Queue<Key> queue = new Queue<Key>();
		for(int i = 0;i < this.getM();i++){
			if(this.getKeys()[i] != null){
				queue.enqueue(this.getKeys()[i]);
			}
		}
		return queue;
	}
	
	//integrity check - don't check after each put() 
	private boolean check() {
		
		//check that hash table is at most 50%
		if(this.getM() < 2 * this.getN()) {
			System.err.println("Hash table size M = " + this.getM() + "; array size N = "  + this.getN());
			return false;
		}
		
		//check that each key in table can be foudn by get () 
		for(int i = 0; i < this.getM(); i ++){
			if(this.getKeys()[i] == null) continue;
			else if(get(this.getKeys()[i]) != this.getVals()[i]) {
				System.err.println("get[" + this.getKeys()[i] + "] = " + get(this.getKeys()[i]) +
						"; vals[i] = " + this.getVals()[i]);
				return false;
			}
		}
		return true;
	}
		
}

